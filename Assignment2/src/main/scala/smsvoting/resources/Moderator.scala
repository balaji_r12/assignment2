package main.scala.smsvoting.resources
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull
import java.util.Date

class Moderator {

   //@Null
   @JsonProperty("id")
   private var _moderatorId:Int = _ 
   
  def moderatorId = _moderatorId 
 
  def moderatorId_= (moderatorId:Int) = _moderatorId = moderatorId 
 
 /*Getters nd sette"rs for userid */
  @NotNull(message="Email address cannot be empty")
  @JsonProperty("email")
 private var _email:String =_ 
   
 // Getter 
 def email = _email
 
 // Setter 
 def email_= (email:String) = _email = email 
 
 
 /*Getters nd setters for userid */
 @NotNull(message="Password cannot be empty")
 @JsonProperty("password")
  private var _password:String =_ 
   
 // Getter 
 def password  = _password 
 
 // Setter 
 def password_= (password:String) = _password = password 
 
 @JsonProperty("name")
 private var _name:String =_ 
   
 // Getter 
 def name  = _name
 
 // Setter 
 def name_= (name:String):Unit = _name= name
 
 @JsonProperty("created_at")
private var _created_at:String=_

//Getter
def created_at=_created_at

//setter
def created_at_=(created_at:String):Unit = _created_at=created_at
}

package main.scala.smsvoting


import main.scala.smsvoting.resources.Poll;
import main.scala.smsvoting.resources.MongoDataAccess; 
import main.scala.smsvoting.resources.Moderator
import org.springframework.web.bind.annotation.{RequestMapping, RestController}
import org.springframework.context.annotation.{Configuration, ComponentScan}
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.http.{ResponseEntity, HttpStatus}
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.client.ResponseExtractor
import org.springframework.web.filter.ShallowEtagHeaderFilter
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.http.HttpRequest
import org.springframework.web.context.request.WebRequest
import org.springframework.http.HttpEntity
import main.scala.smsvoting.resources.Moderator
import java.util.Calendar
import java.util.Date
import scala.collection.mutable.Map
import collection.JavaConverters._
import javax.validation.Valid
import java.text.SimpleDateFormat
import java.util.HashMap
import java.util.Map
import java.util.Arrays.ArrayList
import java.security.Security
import java.util.Formatter.DateTime
import org.apache.commons.codec.binary.Base64
import sun.misc.BASE64Decoder
import java.lang.Boolean
import com.mongodb.casbah.Imports._


@JsonIgnoreProperties(ignoreUnknown=true)
@RestController
@EnableAutoConfiguration
class SMSVoting {

   val userMap = collection.mutable.HashMap[Int,Moderator]()
   val pollMap = collection.mutable.HashMap[String,Poll]()
   var moderatorId:Int=0;
   
   var getUserPollList = new java.util.ArrayList[Poll]()
   val moderatorpollMap = collection.mutable.HashMap[Int,java.util.ArrayList[Poll]]()
   val mongoAccess= new MongoDataAccess();
   
  /*sample to print helloworld*/
  @RequestMapping(value=Array("/api/v1"))
     def test():String = {
       "Congratulations. API is working!!"
     
    }
     
   //Create Moderator
   @RequestMapping(value=Array("/api/v1/moderators"),method = Array(RequestMethod.POST))
   @ResponseStatus(value = HttpStatus.CREATED)
     def createUser(@Valid @RequestBody moderator:Moderator,req:WebRequest) = {
         println("entered create moderator")
         val today = Calendar.getInstance().getTime()
         val dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
         val formattedDate:String =dateFormat.format(today);
         //val created_at = new java.sql.Timestamp(System.currentTimeMillis()).toString();
          //val random= (Math.round(Math.random() * 89999) + 10000);
          moderatorId+=1;
          var random=moderatorId;
          println("input moderator email"+moderator.email)
          if(moderator!=null)
          {
             moderator.email_=(moderator.email)
             println("moderator email"+moderator.email)
             moderator.moderatorId=(random.toInt)
             moderator.password_=(moderator.password)
             moderator.name_=(moderator.name)
             moderator.created_at_=(formattedDate)
             mongoAccess.CreateUser(moderator);
             println("created moderator id"+ moderator.moderatorId)
             moderator
          }
          else
          {
                new ResponseEntity("Moderator empty",null,HttpStatus.BAD_REQUEST)
          }
     
    }
 
  //authentication method
  private def AuthenticateUser(auth:String):Boolean=
 { 
    var authenticated=false
     decodeBasicAuth(auth) match {
                case Some((user, pass)) => {
                      if (user == "foo" && pass == "bar") {
                         authenticated=true
                               
                    }
                    else
                    {
                      authenticated=false
                    }
                }
                case _ => ;
            }
    authenticated
 }
  private def decodeBasicAuth(auth: String): Option[(String, String)] = {
        lazy val basicSt = "basic " 
        val basicReqSt = auth.substring(0, basicSt.length())
        if (basicReqSt.toLowerCase() != basicSt) {
            return None
        }
        val basicAuthSt = auth.replaceFirst(basicReqSt, "")
        //BESE64Decoder is not thread safe, don't make it a field of this object
        val decoder = new BASE64Decoder()
        val decodedAuthSt = new String(decoder.decodeBuffer(basicAuthSt), "UTF-8")
        val usernamePassword = decodedAuthSt.split(":")
        println("decoded password"+usernamePassword);
        if (usernamePassword.length >= 2) {
            //account for ":" in passwords
            return Some(usernamePassword(0), usernamePassword.splitAt(1)._2.mkString)
        }
        None
    }
      
   //View moderator
   @RequestMapping(value=Array("/api/v1/moderators/{moderator_id}"),method = Array(RequestMethod.GET))
     def getUser(@PathVariable moderator_id:Int,req:WebRequest) = {
      println("view moderator called for id"+moderator_id)
    // val authHeader=req.getHeader("Authorization")
     if(true)
       {
       println("user authorized for get user")
       var moderator= mongoAccess.GetModerator(moderator_id)
       if(moderator!=null)
        {
           val header = new HttpHeaders()
           println("returning get user for moderator id"+moderator_id)
           new ResponseEntity(moderator,header,HttpStatus.OK)
     
        }
       else
         {
           
            println("view moderator failed. you are not a user. Input Moderator Id"+moderator_id)
           "You are not a user"
         }
       }
      else
       {
             println("view moderator failed. unauthorized")
           new ResponseEntity("Unauthorized access",null,HttpStatus.UNAUTHORIZED)
       }
   }
   
   //Update user
   @RequestMapping(value=Array("/api/v1/moderators/{moderatorId}"),method=Array(RequestMethod.PUT))
    @ResponseStatus(value = HttpStatus.CREATED)
   def UpdateUser(@PathVariable moderatorId:Int, @Valid @RequestBody moderator:Moderator,req:WebRequest)={
     
     val authHeader=req.getHeader("Authorization")
     if(true)
     {
      // println("moderator: "+moderatorId)
       if(moderatorId > 0)
       {
            val updatedModerator= mongoAccess.UpdateModerator(moderator, moderatorId)
            new ResponseEntity(updatedModerator,null,HttpStatus.OK)
            
       }
       else
       {
         new ResponseEntity("Invalid user Id. No user exists",null,HttpStatus.BAD_REQUEST)
       }
     }
     else
       {
           new ResponseEntity("Unauthorized access",null,HttpStatus.UNAUTHORIZED)
       }
     
   }
   
   //Create Poll
   @RequestMapping(value=Array("/api/v1/moderators/{moderator_Id}/polls"),method = Array(RequestMethod.POST))
   @ResponseStatus(value = HttpStatus.CREATED)
     def createPoll(@Valid @RequestBody poll:Poll,@PathVariable moderator_Id:Int,req:WebRequest) = {
     val authHeader=req.getHeader("Authorization")
     println("create poll called")
     if(true)
     {
         println("user authorized for create poll")
       if(true) {
         println("user exists for create poll")
         val random= (Math.round(Math.random() * 89999) + 10000).toString()
         poll.question_=(poll.question)
         poll.Id=(random)
         //poll.moderator_Id_=(moderator_Id)
         poll.started_at_=(poll.started_at)
         poll.choice_=(poll.choice)
         poll.expired_at_=(poll.expired_at)
         mongoAccess.CreatePoll(moderator_Id, poll)
         println("poll created for moderator"+moderator_Id)
         
       } 
       poll 
     }
      else
       {
           new ResponseEntity("Unauthorized access",null,HttpStatus.UNAUTHORIZED)
       }
     
    }
   
    //View a poll without results
     @RequestMapping(value=Array("/api/v1/polls/{poll_id}"),method = Array(RequestMethod.GET))
     def ViewPollWithNoResult(@PathVariable poll_id:String,req:WebRequest) = {
        
     if(poll_id !=null)
      {
         val poll= mongoAccess.GetPollWithNoResult(poll_id)
         println("returned poll"+poll);
         new ResponseEntity(poll,null,HttpStatus.OK)
   
      }
     else
       {
         "No record exists for the requested poll"
       }
     }
     
     
       //View a poll with results
     @RequestMapping(value=Array("/api/v1/moderators/{moderator_id}/polls/{poll_id}"),method = Array(RequestMethod.GET))
     def ViewPollWithResult(@PathVariable moderator_id:Int,@PathVariable poll_id:String,req:WebRequest) = {
     
     val authHeader=req.getHeader("Authorization")
     if(true)
     {
       if(true)
        {
           val poll=mongoAccess.GetPollWithResults(moderator_id,poll_id)
           val header = new HttpHeaders()
           val arr:Array[String]=Array("500","600")
           poll.results=arr
           new ResponseEntity(poll,header,HttpStatus.OK)
     
        }
       else
         {
           "No record exists for the requested poll"
         }
     }
     else
       {
           new ResponseEntity("Unauthorized access",null,HttpStatus.UNAUTHORIZED)
       }
     }
     
      //List all poll
     @RequestMapping(value=Array("/api/v1/moderators/{moderator_id}/polls"),method = Array(RequestMethod.GET))
     def ListAllPoll(@PathVariable moderator_id:Int,req:WebRequest) = {
     
     val authHeader=req.getHeader("Authorization")
     if(true)
     {
       if(true)
        {
           val header = new HttpHeaders()
           println("going to mongo for moderator id:"+moderator_id)
           val pollList=mongoAccess.GetAllPollsWithResults(moderator_id)
           new ResponseEntity(pollList,header,HttpStatus.OK)
     
        }
       else
         {
           "No poll exists for this moderator"
         }
       }
     else
       {
           new ResponseEntity("Unauthorized access",null,HttpStatus.UNAUTHORIZED)
       }
     }
     
    //DELETE POLL 
     @RequestMapping(value=Array("/api/v1/moderators/{moderator_id}/polls/{poll_id}"),method = Array(RequestMethod.DELETE))
     @ResponseStatus(value = HttpStatus.NO_CONTENT)
     def deletePoll(@PathVariable moderator_id:Int,@PathVariable poll_id:String,req:WebRequest) = {
     val authHeader=req.getHeader("Authorization")
     if(true)
     {
      if(true)
      {
        mongoAccess.DeletePoll(poll_id, moderator_id)
        new ResponseEntity(HttpStatus.NO_CONTENT)
      }
       else
       {
         "No Poll exists to delete"
       }
      }
     else
       {
           new ResponseEntity("Unauthorized access",null,HttpStatus.UNAUTHORIZED)
       }
     }
     
    //VOTE A POLL
   @RequestMapping(value=Array("/api/v1/polls/{poll_id}"),method=Array(RequestMethod.PUT))
   @ResponseStatus(value = HttpStatus.NO_CONTENT)
   def Vote(@PathVariable poll_id:String,req:WebRequest)={
        
     if(true)
     {
          var choice=0;
          if(req.getParameter("choice")=="0")
          choice=0
          else
          choice=1
          mongoAccess.VotePoll(poll_id, choice)
          new ResponseEntity(HttpStatus.NO_CONTENT)
     }
     else
     {
      
       new ResponseEntity("No poll exists",null,HttpStatus.BAD_REQUEST)
     }
    
     
   }
     
}
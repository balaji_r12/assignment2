package main.scala.smsvoting.resources
import com.mongodb.casbah.Imports._
import com.mongodb.DBObject
import java.sql.Date
import com.mongodb.util.JSONSerializers.DBObjectSerializer
import java.text.SimpleDateFormat
import java.util.Formatter.DateTime
import java.util.Date
import java.lang.reflect.Array
import java.util.ArrayList
import com.mongodb.BasicDBList
import com.mongodb.BasicDBObject


class MongoDataAccess {

  private val uri = """mongodb://root:root@ds035750.mongolab.com:35750/mongodev"""
  val db = MongoClient(MongoClientURI(uri))( """mongodev""")
  val collection = db("ModeratorPoll")
  
  def convertModeratorToMongoObject(moderator: Moderator): DBObject = {
    MongoDBObject(
      "moderatorId" -> moderator.moderatorId,
      "email" -> moderator.email,
      "created_at" -> moderator.created_at,
      "password" -> moderator.password,
      "name" -> moderator.name
    )
  }
  
  def convertPolltoMongoObject(poll: Poll): DBObject = {
    MongoDBObject(
      "id" -> poll.Id,
      "question" -> poll.question,
      "started_at" -> poll.started_at,
      "expired_at" -> poll.expired_at,
      "choice" -> poll.choice
  
    )
  }

    
  def CreateUser(moderator:Moderator)
  {
     collection.insert(convertModeratorToMongoObject(moderator));

    
  }
  
  def CreatePoll(moderatorId:Int,poll:Poll)
  {
    val query = MongoDBObject("moderatorId" -> moderatorId)
    val moderatorObj=collection.findOne(query);

    val pollListDBList= new BasicDBList();
    pollListDBList.add(convertPolltoMongoObject(poll))
  
    for(x <- moderatorObj){
         x+="polls"->pollListDBList
         collection.save(x)
     }
  }

   def GetModerator(moderatorId:Int):Moderator=
  {
    val moderator=new Moderator();
    val query = MongoDBObject("moderatorId" -> moderatorId)
    val moderatorObj=collection.findOne(query);

     for(x <- moderatorObj){
      moderator.moderatorId = x.getAsOrElse[Int]("moderatorId", 0);
      moderator.name = x.getAsOrElse[String]("name", "N/A")
      moderator.email = x.getAsOrElse[String]("email", "N/A")
      moderator.created_at = x.getAsOrElse[String]("created_at", "N/A")
      moderator.password = x.getAsOrElse[String]("password", "N/A")
      }
     moderator;
   
  }
   
  def GetPollWithNoResult(pollId:String):Poll=
  {
    val poll=new Poll();
    println("finding poll id"+ pollId)
    val query = MongoDBObject("polls.id"->pollId);
    val pollObj=collection.findOne(query);
    println("poll oject"+pollObj)
    var pollListDBList= new BasicDBList();
    if (pollObj.get("polls") != null){
        pollListDBList =  pollObj.get("polls").asInstanceOf[BasicDBList]
     }
    val size = pollListDBList.size();
    println("poll object size"+size)
    for (i <- 0 to pollListDBList.size()-1) 
     {
        val str=pollListDBList.get(i).asInstanceOf[BasicDBObject]
        val question = str.get("question")
        poll.Id = str.get("id").toString();
        poll.question = str.get("question").toString();
        poll.started_at = str.get("started_at").toString();
        poll.expired_at =str.get("expired_at").toString();
        var choiceString=str.get("choice")
        val arrchoice=choiceString.toString().split(",")
        poll.choice=arrchoice
   
     }
     poll;
  }
  
  
  def GetPollWithResults(moderatorId:Int,pollId:String):Poll=
  {
    val poll=new Poll();
    println("finding poll id"+ pollId)
    val query = MongoDBObject("polls.id"->pollId);
    val pollObj=collection.findOne(query);
    println("poll oject"+pollObj)
    var pollListDBList= new BasicDBList();
    if (pollObj.get("polls") != null){
        pollListDBList =  pollObj.get("polls").asInstanceOf[BasicDBList]
     }
    val size = pollListDBList.size();
    println("poll object size"+size)
    for (i <- 0 to pollListDBList.size()-1) 
     {
        val str=pollListDBList.get(i).asInstanceOf[BasicDBObject]
        val question = str.get("question")
        poll.Id = str.get("id").toString();
        poll.question = str.get("question").toString();
        poll.started_at = str.get("started_at").toString();
        poll.expired_at =str.get("expired_at").toString();
        var choiceString=str.get("choice")
        val arrchoice=choiceString.toString().split(",")
        poll.choice=arrchoice
        var resultString=str.get("results")
        val arrresult=choiceString.toString().split(",")
        poll.results=arrresult
   
     }
     poll;
     
  }

    def UpdateModerator(updatedmoderator:Moderator, moderatorId:Int):Moderator=
  {

    val moderator=new Moderator();
    println("new email:"+updatedmoderator.email)
    val query = MongoDBObject("moderatorId" -> moderatorId)
    val update=$set("email"->updatedmoderator.email,"password"->updatedmoderator.password)
    val updatedObject = collection.update(query,update)
    val moderatorObj=collection.findOne(query);

     for(x <- moderatorObj){
      moderator.moderatorId = x.getAsOrElse[Int]("moderatorId", 0);
      moderator.name = x.getAsOrElse[String]("name", "N/A")
      moderator.email = x.getAsOrElse[String]("email", "N/A")
      moderator.created_at = x.getAsOrElse[String]("created_at", "N/A")
      moderator.password = x.getAsOrElse[String]("password", "N/A")
      }
     moderator;
  }
    
  def GetAllPollsWithResults(moderatorId:Int):java.util.ArrayList[Poll]=
  {
    var pollList=new java.util.ArrayList[Poll]();
    val query = MongoDBObject("moderatorId" -> moderatorId)
    val moderatorObj=collection.findOne(query);
    var pollListDBList= new BasicDBList();
    if (moderatorObj.get("polls") != null){
        pollListDBList =  moderatorObj.get("polls").asInstanceOf[BasicDBList]
     }
    var size = pollListDBList.size();
    for (i <- 0 to size-1) 
     {
       val str=pollListDBList.get(i).asInstanceOf[BasicDBObject]
       val question = str.get("question")
       val poll=new Poll();
        poll.Id = str.get("id").toString();
        poll.question = str.get("question").toString();
        poll.started_at = str.get("started_at").toString();
        poll.expired_at =str.get("expired_at").toString();
        var choiceString=str.get("choice")
        val arrchoice=choiceString.toString().split(",")
        poll.choice=arrchoice
        pollList.add(poll)

     }
     pollList;
   
  }
  
   def DeletePoll(pollId:String,moderatorId:Int)
  {
   
   val modobj = new BasicDBObject("moderatorId", moderatorId) // to match your document
   var update = new BasicDBObject("polls", new BasicDBObject("id", pollId))
   collection.update(modobj, new BasicDBObject("$pull", update))

  }  
   

   def VotePoll(pollId:String,choice:Int)
  {
     
    val query = MongoDBObject("polls.id" -> pollId)
    val pollObj=collection.findOne(query);

    val obj = new BasicDBObject("polls.$.results", new BasicDBObject("score","500"))
    collection.update(query, new BasicDBObject("$push",obj));
    
    val obj1 = new BasicDBObject("polls.$.results", new BasicDBObject("score","600"))
    collection.update(query, new BasicDBObject("$push",obj1));
     
  }
  



}
package main.scala.smsvoting.resources
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull
import java.sql.Date
import com.sun.xml.internal.fastinfoset.util.StringArray
import java.util.List


class Poll {

   @JsonProperty("id")
   private var _Id:String = _ 
    def Id = _Id 
    def Id_= (Id:String) = _Id = Id 

   @NotNull(message="Questions cannot be empty for the poll")
   @JsonProperty("question")
   private var _question:String= _
   def question=_question
   def question_=(question:String)=_question=question
   
   @NotNull(message="Start date cannot be empty for the poll") 
   @JsonProperty("started_at")
   private var _started_at:String= _
   def started_at=_started_at
   def started_at_=(started_at:String)=_started_at=started_at
   
   @NotNull(message="Expiration date cannot be empty for the poll") 
   @JsonProperty("expired_at")
   private var _expired_at:String= _
   def expired_at=_expired_at
   def expired_at_=(expired_at:String)=_expired_at=expired_at
   
   @NotNull(message="Invalid choice. Please enter a valid choice") 
   @JsonProperty("choice")
   private var _choice:Array[String]= _
   def choice=_choice
   def choice_=(choice:Array[String])=_choice=choice
   
 
   @JsonProperty("results")
   private var _results:Array[String]= _
   @JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
   def results=_results
   def results_=(results:Array[String])=_results=results 
  
}